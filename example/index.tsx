import "react-app-polyfill/ie11";
import * as React from "react";
import * as ReactDOM from "react-dom";
import {
  Button,
  Space,
  Tag,
  Switch,
  Modal,
  Carousel,
  Progress,
  ProgressStatus,
} from "../.";

const Wrapper: React.FC<React.PropsWithChildren<{
  top?: number;
  topLine?: boolean;
}>> = ({ children, topLine, top = 10 }) => {
  return (
    <div
      style={{ marginTop: top, borderTop: topLine ? "1px solid #212121" : "" }}>
      {children}
    </div>
  );
};

const App = () => {
  const [state, setState] = React.useState(false);
  const [visible, setVisible] = React.useState(false);
  const contentStyle: React.CSSProperties = {
    margin: 0,
    height: "160px",
    color: "#fff",
    lineHeight: "160px",
    textAlign: "center",
    background: "#364d79",
  };
  const [progressState, setProgressstate] = React.useState({
    percent: 30,
    status: "active",
  });

  React.useEffect(() => {
    let timeout: NodeJS.Timeout;
    timeout = setInterval(() => {
      const randomCount = Math.random() * 10;
      setProgressstate((prev) => {
        if (prev.percent >= 100) {
          return { ...prev, percent: 0 };
        }
        return { ...prev, percent: prev.percent + randomCount };
      });
    }, 1500);

    return () => clearInterval(timeout);
  }, []);

  return (
    <Wrapper>
      <Wrapper>
        <Space size="small">
          <Tag color="success">成功</Tag>
          <Tag color="warning">警告</Tag>
          <Tag color="processing">processing</Tag>
          <Tag color="default">默认</Tag>
          <Tag color="error">错误</Tag>
          <Tag color="#2db7f5">自定义颜色</Tag>
        </Space>
      </Wrapper>
      <Wrapper>
        <Space size={"small"} align={"center"}>
          <Button
            type="primary"
            size="large"
            onClick={() => {
              console.log("this is button");
            }}>
            按钮
          </Button>
          <Button
            type="warning"
            size="middle"
            onClick={() => {
              console.log("this is button");
            }}>
            按钮
          </Button>
          <Button
            type="info"
            size="small"
            onClick={() => {
              console.log("this is button");
            }}>
            按钮
          </Button>
          <Button disabled size="small">
            禁用按钮
          </Button>
        </Space>
      </Wrapper>
      <Wrapper>
        <Space align="center">
          <Button
            onClick={() => {
              setState(!state);
            }}>
            切换
          </Button>
          <Switch
            openText={"开启"}
            closeText={"关闭"}
            checked={state}
            onChange={(state) => {
              console.log("state", state);
            }}
          />
        </Space>
      </Wrapper>

      <Wrapper>
        <Button type="primary" onClick={() => setVisible(true)}>
          显示Modal
        </Button>
        <Modal
          title={"标题"}
          open={visible}
          mask={true}
          okText={"确定"}
          cancelText={"取消"}
          onCancel={() => setVisible(false)}
          onOk={() => {
            console.log("确定");
          }}>
          <Carousel>
            <h3 style={contentStyle}>1</h3>
            <div>
              <h3 style={contentStyle}>2</h3>
            </div>
            <div>
              <h3 style={contentStyle}>3</h3>
            </div>
            <div>
              <h3 style={contentStyle}>4</h3>
            </div>
            <div>
              <h3 style={contentStyle}>5</h3>
            </div>
            <div>
              <h3 style={contentStyle}>6</h3>
            </div>
            <div>
              <h3 style={contentStyle}>7</h3>
            </div>
          </Carousel>
        </Modal>
      </Wrapper>
      <Wrapper top={30}>
        <Wrapper>
          <Space>
            <Button
              onClick={() =>
                setProgressstate((prev) => {
                  return { ...prev, percent: prev.percent - 10 };
                })
              }>
              -
            </Button>
            <Progress
              width={320}
              toFixed={2}
              percent={progressState.percent}
              status={progressState.status as ProgressStatus}
            />
            <Button
              style={{ marginLeft: 30 }}
              onClick={() =>
                setProgressstate((prev) => {
                  return { ...prev, percent: prev.percent + 10 };
                })
              }>
              +
            </Button>
            <Space>
              <Button
                onClick={() =>
                  setProgressstate((prev) => {
                    return { ...prev, status: "success" };
                  })
                }>
                完成
              </Button>
              <Button
                type="info"
                onClick={() =>
                  setProgressstate((prev) => {
                    return { ...prev, status: "active" };
                  })
                }>
                正常
              </Button>
              <Button
                type="warning"
                onClick={() =>
                  setProgressstate((prev) => {
                    return { ...prev, status: "exception" };
                  })
                }>
                异常
              </Button>
            </Space>
          </Space>
        </Wrapper>
      </Wrapper>
    </Wrapper>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
