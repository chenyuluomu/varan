import type { CSSProperties, FC, PropsWithChildren } from "react";
import React from "react";
import classNames from "classnames";
import styles from "./style.module.css";

type BtnTypes = "primary" | "warning" | "info";

interface ButtonState {
  type: BtnTypes;
  block: boolean;
  disabled: boolean;
  size: "large" | "middle" | "small";
  style: CSSProperties;
  onClick: () => void;
}

export type ButtonStates = Partial<ButtonState>;

export const Button: FC<PropsWithChildren<ButtonStates>> = ({
  type,
  block,
  disabled,
  onClick,
  size,
  style,
  children,
}) => {
  const btnType = styles[`varan-btn-${type}`];
  const btnSize = styles[`varan-btn-${size}`];

  const btnClass = classNames("varan-btn", {
    "varan-btn-disabled": disabled && true,
  })
    .split(" ")
    .map((i) => styles[i]);

  return (
    <button
      disabled={disabled}
      style={block ? { width: "100%", ...style } : { ...style }}
      className={classNames(btnClass, btnType, btnSize)}
      onClick={onClick}>
      {children}
    </button>
  );
};

Button.defaultProps = {
  type: "primary",
  block: false,
  disabled: false,
  size: "middle",
};
