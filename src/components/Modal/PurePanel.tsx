import * as React from "react";
import type { ModalProps } from "./index";
import { Button, Space } from "../../";

export const renderFooter = (
  props: Pick<ModalProps, "footer" | "okText" | "cancelText"> & {
    onOk?: React.MouseEventHandler | any;
    onCancel?: React.MouseEventHandler | any;
  }
) => {
  const { footer, okText, cancelText, onOk, onCancel } = props;

  return footer === undefined ? (
    <Space>
      <Button type="info" onClick={onCancel}>
        {cancelText || "取消"}
      </Button>
      <Button type="primary" onClick={onOk}>
        {okText || "确定"}
      </Button>
    </Space>
  ) : (
    footer
  );
};
