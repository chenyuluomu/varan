import React from "react";
import Dialog from "rc-dialog";
// import classNames from "classnames";
import "rc-dialog/assets/index.css";
import { renderFooter } from "./PurePanel";
// import styles from "./style.module.css";

export interface ModalProps extends React.PropsWithChildren {
  title: React.ReactNode;
  open: boolean;
  visible: boolean;
  onOk: (e: React.MouseEvent<HTMLElement>) => void;
  onCancel: (e: React.MouseEvent<HTMLElement>) => void;
  okText: React.ReactNode;
  cancelText: React.ReactNode;
  footer: React.ReactNode | undefined;
  mask: boolean;
  maskClosable: boolean;
  width: number;
}

export const Modal: React.FC<Partial<ModalProps>> = (props) => {
  const {
    title,
    children,
    visible,
    open,
    mask,
    maskClosable,
    width = 520,
    ...resetProps
  } = props;

  const handleCancel = (e: React.MouseEvent<HTMLElement> | any) => {
    const { onCancel } = props;
    onCancel?.(e);
  };

  const handleOk = (e: React.MouseEvent<HTMLElement> | any) => {
    const { onOk } = props;
    onOk?.(e);
  };

  return (
    <Dialog
      {...resetProps}
      width={width}
      title={title}
      mask={mask}
      maskClosable={maskClosable}
      visible={open ?? visible}
      onClose={handleCancel}
      footer={renderFooter({
        ...props,
        onOk: handleOk,
        onCancel: handleCancel,
      } as any)}>
      {children}
    </Dialog>
  );
};

Modal.defaultProps = {
  title: "默认标题",
  mask: true,
  maskClosable: true,
};
