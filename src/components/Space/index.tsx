import React from "react";
import type { FC, PropsWithChildren } from "react";
import classNames from "classnames";
import styles from "./style.module.css";

export type SpaceState = Partial<{
  align: "start" | "end" | "center" | "baseline";
  size: "large" | "middle" | "small";
}>;

export const Space: FC<PropsWithChildren<SpaceState>> = ({
  size,
  align,
  children,
}) => {
  const spaceType = styles[`varan-space-${size}`];
  const spaceAlign = styles[`varan-space-${align}`];

  return (
    <div className={classNames(styles["varan-space"], spaceType, spaceAlign)}>
      {React.Children.map(children, (ele) => {
        return <div className={styles["varan-space-item"]}>{ele}</div>;
      })}
    </div>
  );
};

Space.defaultProps = {
  align: "center",
  size: "small",
};
