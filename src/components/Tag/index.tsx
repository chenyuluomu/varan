import React from "react";
import type { FC, PropsWithChildren } from "react";
import classNames from "classnames";
import styles from "./style.module.css";

export interface TagState {
  color: "success" | "processing" | "error" | "default" | "warning" | string;
}

export const Tag: FC<PropsWithChildren<Partial<TagState>>> = ({
  color,
  children,
}) => {
  const tagType = styles[`varan-tag-${color}`];
  const colorType = ["success", "processing", "error", "default", "warning"];
  return (
    <div
      style={
        !colorType.includes(color as string)
          ? {
              background: color,
              color: "#fff",
            }
          : {}
      }
      className={classNames(styles["varan-tag"], tagType)}>
      {children}
    </div>
  );
};

Tag.defaultProps = {
  color: "success",
};
